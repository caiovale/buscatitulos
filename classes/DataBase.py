from classes import MetaSingleton
import pyodbc

class DataBase(object):
    __metaclass__ = MetaSingleton

    def __init__(self, dsn, uid, pwd):
        self.dsn = dsn
        self.uid = uid
        self.pwd = pwd
        self.conexao = pyodbc.connect('DSN='+self.dsn+';'+'UID='+self.uid+';'+'PWD='+self.pwd)
        self.cursor = self.conexao.cursor()

    def popula_tabela(self, nome_arquivo, data_arquivo, critica):
        query = "INSERT INTO excd_cobranca..EXC_preCriticaRetorno VALUES('%s', '%s', '%s')" % \
              (nome_arquivo, data_arquivo, critica)
        self.cursor.execute(query)
        self.cursor.commit()

    def buscar_valor_titulo(self, titulo):
        query = "SELECT vlr_prem_tot FROM Tkgs_Corp..ems_parcela where nro_docto = '%s'" % (titulo)
        self.cursor.execute(query)
        valor_titulo = self.cursor.fetchone()
        if valor_titulo == None:
            valor_titulo = 'Não Consta na tabela EMS_PARCELA do Coreon'
            return valor_titulo
        else:
            return str(valor_titulo[0])

    def buscar_valor_titulo_pago(self, titulo):
        query = "SELECT vlr_pago FROM Tkgs_Corp..ems_parcela where nro_docto = '%s'" % (titulo)
        self.cursor.execute(query)
        valor_titulo = self.cursor.fetchone()
        if valor_titulo == None:
            valor_titulo = 'Não Consta na Tabela EMS_PARCELA do Coreon'
            return valor_titulo
        else:
            return str(valor_titulo[0])

    def buscar_produto(self, titulo):
        query = "select prd.dcr_prod from Tkgs_Corp..ems_parcela pa \
                    inner join Tkgs_Corp..ems_emissao em on pa.cod_ctrt = em.cod_ctrt \
                    inner join Tkgs_Corp..prd_produto prd on em.cod_prod = prd.cod_prod \
                    where pa.nro_docto = '%s'" % (titulo)
        self.cursor.execute(query)
        produto = self.cursor.fetchone()
        if produto == None:
            produto = 'Não Consta na Base do Coreon'
            return produto
        else:
            return str(produto[0])
