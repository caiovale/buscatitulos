from importlib import import_module


def obj_builder(obj_name, *args, **kwargs):

    if '.' in obj_name:
        module_name, class_name = obj_name.rsplit('.', 1)
    else:
        module_name = obj_name

    module = import_module('classes.' + module_name, package = 'classes')

    obj_class = getattr(module, module_name)

    instance = obj_class(*args, **kwargs)

    return instance
