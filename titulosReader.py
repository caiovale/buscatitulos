import time
from os import scandir
from classes import *
import datetime
from tqdm import tqdm

def get_arquivos(path):
    dir_entries = scandir(path)
    lista_arquivos = []
    for arquivo in dir_entries:
        if arquivo.is_file():
            lista_arquivos.append(arquivo.name)
    return lista_arquivos

def buscar_retornos(arquivos, path, titulo_remessa):
    resultado = {'titulo_pago': 'Não'}
    resultado['data_ocorrencia_ret'] = 'Não Encontrado'
    resultado['nome_arquivo_retorno'] = 'Não Encontrado'
    a = 1
    for arquivo in tqdm(arquivos, desc = 'Buscando Título em Arquivos de Retorno'):
            arquivo_retorno_1 = open(path+"/"+arquivo, 'r')
            id_tipo_retorno = arquivo_retorno_1.read(2)
            if(id_tipo_retorno == '02'):
                qtd_linhas_arquivo = len(arquivo_retorno_1.readlines())-2
                arquivo_retorno = open(path + "/" + arquivo, 'r')
                lines = arquivo_retorno.readlines()
                time.sleep(0.01)
                for a in range(qtd_linhas_arquivo):

                    #captura título retorno#
                    tit = lines[a][:73]
                    titulo_retorno = tit[-9:]
                    #captura título retorno#

                    if(titulo_retorno == titulo_remessa):
                        resultado['nome_arquivo_retorno'] = arquivo
                        #captura data ocorrencia retorno#
                        datoc = lines[a][:116]
                        data_ocorrencia_ret = datoc[-6:]
                        date_time_str = data_ocorrencia_ret
                        date_time_obj = datetime.datetime.strptime(date_time_str, '%d%m%y')
                        data_ocorrencia_ret = str(date_time_obj.date())
                        resultado['data_ocorrencia_ret'] = data_ocorrencia_ret
                        #captura data ocorrencia retorno#

                        #captura ocorrencia retorno#
                        oc = lines[a][:110]
                        resultado['ocorrencia_retorno'] = oc[-2:]
                        #captura ocorrencia retorno#

                        if(resultado['ocorrencia_retorno'] == '21'):
                            resultado['titulo_pago'] = 'Sim'
                            a+=1
                        a+=1
                    else:
                        a+=1
    return resultado

def buscar_remessas(arquivos, path, titulo):
    resultado = {'titulo:': None, 'nome_arquivo:': None, 'data_envio_rem': None, 'ocorrencia_rem': None, 'data_vencimento': None}
    a = 1
    for arquivo in tqdm(arquivos, desc = 'Buscando Título em Arquivos de Remessa'):
        arquivo_remessa = open(path + "/" + arquivo, 'r', encoding = 'utf-8', errors = 'ignore')
        qtd_linhas_arquivo = len(arquivo_remessa.readlines()) - 2
        arquivo_remessa = open(path + "/" + arquivo, 'r', encoding = 'utf-8', errors = 'ignore')
        lines = arquivo_remessa.readlines()
        time.sleep(0.01)

        # capturando data do envio#
        datenv = lines[0][:100]
        data_envio = datenv[-6:]
        date_time_obj = datetime.datetime.strptime(data_envio, '%d%m%y')
        data_envio = str(date_time_obj.date())
        # capturando data do envio#

        for a in range(qtd_linhas_arquivo):
            #capturando número do título#
            tit = lines[a+1][:119]
            titulo_remessa = tit[-9:]
            #capturando número do título#

            #capturando ocorrencia remessa#
            oc = lines[a+1][:110]
            ocorrencia_rem = oc[-2:]
            #capturando ocorrencia remessa#

            #capturando data de vencimento#
            vc = lines[a+1][:126]
            vencimento = vc[-6:]
            date_time_str = vencimento
            date_time_obj = datetime.datetime.strptime(date_time_str, '%d%m%y')
            vencimento = str(date_time_obj.date())
            #capturando data de vencimento#
            if(titulo == titulo_remessa):
                resultado['nome_arquivo'] = arquivo
                resultado['data_envio_rem'] = data_envio
                resultado['ocorrencia_rem'] = ocorrencia_rem
                resultado['titulo'] = titulo
                resultado['data_vencimento'] = vencimento
                a+=1
                return resultado
        else:
            a+=1


print('   ___                      _ _          _____ _ _         _           ')
print('  / __\___  _ __  ___ _   _| | |_ __ _  /__   (_) |_ _   _| | ___  ___ ')
print(" / /  / _ \| '_ \/ __| | | | | __/ _` |   / /\/ | __| | | | |/ _ \/ __|")
print("/ /__| (_) | | | \__ \ |_| | | || (_| |  / /  | | |_| |_| | | (_) \__ \.")
print("\____/\___/|_| |_|___/\__,_|_|\__\__,_|  \/   |_|\__|\__,_|_|\___/|___/")
print('                                                                       ')

num_tit = input('Número do Título:' )
print('\n')

#data_base = obj_builder('DataBase', 'MSSQL', 'caio.vale.sql', 'naovouesquecerdenovo')
path_remessa = '/home/caiocaldas/Documentos/REMESSA'
arquivos_remessa = get_arquivos(path_remessa)
resultado_remessa = buscar_remessas(arquivos_remessa, path_remessa, num_tit)
#valor_tit = data_base.buscar_valor_titulo(num_tit)
#valor_pago = data_base.buscar_valor_titulo_pago(num_tit)
#produto = data_base.buscar_produto(num_tit)

a = 1
path_retorno = '/home/caiocaldas/Documentos/RETORNO'
for a in range(2):
    arquivos_retorno = get_arquivos(path_retorno)
    path_retorno = '/home/caiocaldas/Documentos/RETORNO/Backup'
    a+=1

resultado_retorno = buscar_retornos(arquivos_retorno, path_retorno, num_tit)

print('\n\n                +RESULTADO+')

print('| Número do Título: '+resultado_remessa['titulo']+'              ')
#print('| Valor do Título: '+valor_tit+'                  |')
print('| Data do Vencimento: '+resultado_remessa['data_vencimento']+'           ')
#print('| Produto: '+produto+'           |')
print('| Data de Envio - Remessa: '+resultado_remessa['data_envio_rem']+'      ')
print('| Nome Arquivo - Remessa: '+resultado_remessa['nome_arquivo']+'        ')
print('| Ocorrencia Remessa: '+resultado_remessa['ocorrencia_rem']+'                   ')
print('| Título Pago: '+resultado_retorno['titulo_pago']+'                         ')
#print('| Valor Pago: '+valor_pago+'                       |')
print('| Nome Arquivo - Retorno: '+resultado_retorno['nome_arquivo_retorno']+'   ')
print('| Data Ocorrencia - Retorno: '+resultado_retorno['data_ocorrencia_ret']+'')

print('\n')













